<?php
namespace Masnug\Database;

use Input, Eloquent;

/**
* HttpQueryTranslator
*/
class HttpQueryTranslator extends Eloquent
{

    protected $input;

    /**
     * Digunakan untuk nama colom yg disamarkan, misal
     * $column_alias = array(
     *  'name' => 'full_name',
     *  'approved' => 'approved_at'
     * );
     */
    protected $column_alias;

    /**
     * Mendifinisikan daftar arg query yg akan diolah, contoh
     * $filterable = array(
     *  'name' => 'contains',
     *  'approved' => array('at', 'after', 'before')
     * );
     */
    protected $filterable;

    /**
     * Mendifinisikan daftar arg query yg akan diolah, contoh
     * $orderable = array('name');
     */
    protected $orderable;

    protected $filter_statements;

    protected $order_statements;

    private $accepted_operator = array(
        'contains', 'prefix',
        'equal', 'min', 'max',
        'at', 'after', 'before',
        'is', 'in'
    );

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    
        $this->input = Input::all();
        $this->sortHttpQueryFilter();
        $this->sortHttpQueryOrder();
    }

    public function sortHttpQueryOrder()
    {
        if (empty($this->orderable)) return;

        foreach ($this->orderable as $alias) {
            $column = isset($this->column_alias[$alias])
                ? $this->column_alias[$alias] : $alias;

            $element_name = $alias . '-order';
            if (
                ! array_key_exists($element_name, $this->input) ||
                empty($this->input[$element_name])
            ) {
                continue;
            }

            $this->order_statements[$element_name] = array(
                'column' => $column,
                'direction' => $this->input[$element_name]
            );
        }

        return $this->order_statements;
    }

    public function scopeApplyOrder($query)
    {
        if (empty($this->order_statements)) {
            return $query;
        }

        foreach ($this->order_statements as $attrs) {
            $column = $attrs['column'];
            $direction = $attrs['direction'];

            $query->orderBy($column, $direction);
        }

        return $query;
    }

    protected function sortHttpQueryFilter()
    {
        if (empty($this->filterable)) return;

        foreach ($this->filterable as $alias => $operators) {
            $column = isset($this->column_alias[$alias])
                ? $this->column_alias[$alias] : $alias;

            $operators = (array)$operators;
            foreach ($operators as $operator) {
                if (! in_array($operator, $this->accepted_operator)) {
                    throw new \Exception('Operator "'.$operator.'" tidak diizinkan.', 1);
                }

                $element_name = $alias . '-' . $operator;
                if (
                    ! array_key_exists($element_name, $this->input) ||
                    empty($this->input[$element_name]) ||
                    (
                        ! is_array($this->input[$element_name]) &&
                        trim($this->input[$element_name]) == ''
                    )
                ) {
                    continue;
                }

                $this->filter_statements[$element_name] = array(
                    'column' => $column,
                    'operator' => $operator,
                    'value' => $this->input[$element_name]
                );
            }
        }

        return $this->filter_statements;
    }

    public function scopeApplyFilter($query)
    {
        if (empty($this->filter_statements)) {
            return $query;
        }

        foreach ($this->filter_statements as $attrs) {
            $column = $attrs['column'];
            $value = $attrs['value'];

            switch ($attrs['operator']) {
                case 'contains':
                    $query->where(function($query) use ($column, $value) {
                        $i = 0;
                        foreach (explode(' ', $value) as $_value) {
                            if ($i == 0) {
                                $query->where($column, 'like', "%{$_value}%");
                            }
                            else {
                                $query->orWhere($column, 'like', "%{$_value}%");
                            }
                            $i++;
                        }
                    });
                    break;

                case 'prefix':
                    $query->where($column, 'like', "{$value}%");
                    break;

                case 'equal':
                case 'at':
                case 'is':
                    $query->where($column, '=', $value);
                    break;

                case 'min':
                case 'after':
                    $query->where($column, '>=', $value);
                    break;

                case 'max':
                case 'before':
                    $query->where($column, '<=', $value);
                    break;

                case 'in':
                    $query->whereIn($column, $value);
                    break;
            }
        }

        return $query;
    }

    public function scopeApplyFilterAndOrder($query)
    {
        $this->scopeApplyOrder($query);
        return $this->scopeApplyFilter($query);

    }

    public function getFilterStatement($key = null)
    {
        if (is_null($key)) {
            return $this->filter_statements;
        }
        if ($key && array_key_exists($key, $this->filter_statements)) {
            return $this->filter_statements[$key];
        }

        return null;
    }

    public function getOrderStatement($key = null)
    {
        if (is_null($key)) {
            return $this->order_statements;
        }
        if ($key && array_key_exists($key, $this->order_statements)) {
            return $this->order_statements[$key];
        }

        return null;
    }
}